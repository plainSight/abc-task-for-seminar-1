format PE console 4.0
 
include 'win32a.inc' 
cinvoke printf, arrsz 
cinvoke scanf, tpi, arr_sz 
mov eax, [arr_sz] 
sal eax, 2 ; array size * sizeof(long) 
invoke malloc, eax 
or eax,eax 
jz gtfo ; malloc error 
mov [arr], eax 
invoke puts, arrelm 
xor esi, esi 
mov edi, [arr] 
@@: cinvoke printf, tpo, esi 
cinvoke scanf, tpi, N 
mov eax, [N] 
stosd 
inc esi 
cmp esi,[arr_sz] 
jb @B 
invoke qsort, [arr], [arr_sz], 4, arr_cmp ; 4 = sizeof(long) 
invoke puts, res 
mov esi, [arr] 
xor edi, edi 
@@: lodsd 
cinvoke printf, tps, edi, eax 
inc edi 
cmp edi, [arr_sz] 
jb @B 
pop eax 
invoke free, [arr] 
invoke _getch 
gtfo: invoke exit,0 
 
; array elements comparison function 
proc arr_cmp c, num2:dword, num1:dword 
mov eax, [num2] 
mov edx, [num1] 
mov eax, [eax] 
cmp eax, [edx] 
setg al 
movzx eax, al 
ret 
endp 
 
arr dd ? 
arr_sz dd ? 
N dd ? 
arrsz db 'Enter array size:',0 
arrelm db 'Enter array elements:',0 
tpo db 'A[%d]=',0 
tps db 'A[%d]=%d',0Dh,0Ah,0 
tpi db '%d',0 
res db 'Sorted array:',0 
data import 
 
library msvcrt,'MSVCRT.DLL' 
 
import msvcrt,\ 
puts,'puts',\ 
printf,'printf',\ 
scanf,'scanf',\ 
exit,'exit',\ 
qsort,'qsort',\ 
malloc,'malloc',\ 
free,'free',\ 
_getch,'_getch' 
end data